= headinganchors.module =

This module takes <h2> and <h3> headings and adds id attributes to them to allow them to be linked to in a URL. The following rules are used to create an attribute:
 * All non alphanumerics are removed
 * Case is preserved to allow for some measure of readability
 * HTML tags are stripped from in the middle of heading tags for the anchors

Example: <h2>I Link To <a href="http://www.csaonline.ca/">csaonline.ca</a></h2>

becomes:
<h2 id="ILinkTocsaonlineca">I Link To <a href="http://www.csaonline.ca/">csaonline.ca</a></h2>
and can be referenced with /<url>#ILinkTocsaonlineca

Multiple headings with the same text will be assigned id's with -#, increasing for each identical id.

This module was built from the filter example on the Drupal website.

= tableofcontents.module =
This is a module to generate a table of contents section based on <h[2-3]> tags. It currently depends on the headinganchors.module for full functionality, otherwise it will use the id's supplied by the user. For an example, see http://www.csaonline.ca/clublist.

== Installation ==
To install, place the contents of the tableofcontents directory in your modules directory. Enable headinganchors.module, and optionally, tableofcontents.module. Then, go to admin/input formats, and add each of the desired filters to your input types. Note that tableofcontents currently requires headinganchors for full functionality!

== Use ==
Simply use <!--tableofcontents--> to insert a table of contents at any point in the document.

No database tables are created. To remove the module, just disable the input formats or the module, and remove <!--tableofcontents--> if desired (however, the tag is an HTML comment so it won't show up in your rendered documents).

== Settings ==
There are a few settings which you can use to control the display of the table of contents. To use them, use the following syntax:

<!--tableofcontents option1: value1; option2: value2;...-->

Valid settings include:
title: Override the default "Table of Contents" title. This setting is piped through t() for translation.
list: Choose an ordered list (ol) or an unordered list (ul). Note that Garland doesn't seem to like nested unordered lists, and displays them flat. Defaults to an ordered list.
minlevel: The minimum heading level for table of contents entries. Defaults to <h2>
maxlevel: The maximum heading level for table of contents entries. Defaults to <h3>

--
Andrew Berry
andrewberry@sentex.net
Developed for http://www.csaonline.ca/
Project Page: http://drupal.org/project/tableofcontents
